<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Rutas de prueba
Route::get('inicio', function () {
    return view('inicio');
})->name('inicio');

Route::get('avisos', function () {
    return view('avisos');
})->name('avisos');

Route::get('/actividades', 'EventoController@index')->name('actividades');
Route::get('/eventos/get', 'EventoController@get_events');
Route::post('/eventos/create', 'EventoController@create_events');
// Route::get('actividades', function () {
//     return view('actividades');
// })->name('actividades');

Route::get('notas', function () {
    return view('notas');
})->name('notas');

Route::get('anotaciones', function () {
    return view('anotaciones');
})->name('anotaciones');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
//Route::post('/', 'LoginController@showLoginForm');
//Route::post('logeo', 'LoginController@login')->name('logeo');
//Route::post('logout', 'LoginController@logout')->name('logout');
