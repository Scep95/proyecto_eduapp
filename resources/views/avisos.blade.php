@extends("theme.$theme.layout")

@section('titulo')
    Avisos
@endsection

@section('contenidoHeader')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Avisos</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                    <li class="breadcrumb-item active">Avisos</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
@endsection

@section('contenido')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <!-- /.card -->
                <div class="card">
                    <div class="card-header">
                    <h3 class="card-title">Búsqueda</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                    </div>
                    </div>
                    <div class="card-body p-0">
                    <ul class="nav nav-pills flex-column">
                        <li class="nav-item nav-link">
                            <div class="form-group">
                                <label>Curso</label>
                                <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">Todos los cursos</option>
                                <option>1° A</option>
                                <option>2° C</option>
                                </select>
                            </div>
                        </li>
                        <li class="nav-item nav-link">
                            <div class="form-group">
                                <label>Asignatura</label>
                                <select class="form-control select2" style="width: 100%;">
                                <option selected="selected">Todas las asignaturas</option>
                                <option>Matemática</option>
                                <option>Física</option>
                                </select>
                            </div>
                        </li>
                        <div class="form-group" align="center">
                            <button class="btn btn-primary" value="Buscar" >
                                <i class="ace-icon fa fa-search bigger-110"></i> Buscar</button>
                        </div>

                    </ul>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <section id="tabs" class="project-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <nav>
                                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-ver-tab" data-toggle="tab" href="#nav-ver" role="tab" aria-controls="nav-home" aria-selected="true">Ver Enviados</a>
                                        <a class="nav-item nav-link" id="nav-enviar-tab" data-toggle="tab" href="#nav-enviar" role="tab" aria-controls="nav-profile" aria-selected="false">Enviar Avisos</a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-ver" role="tabpanel" aria-labelledby="nav-home-tab">
                                        <div class="card card-primary card-outline">
                                            <div class="card-header">
                                                <h3 class="card-title">Avisos enviados</h3>

                                                <div class="card-tools">
                                                <div class="input-group input-group-sm">
                                                    <input type="text" class="form-control" placeholder="Buscar avisos">
                                                    <div class="input-group-append">
                                                    <div class="btn btn-primary">
                                                        <i class="fas fa-search"></i>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                                <!-- /.card-tools -->
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body p-0">
                                                <div class="mailbox-controls">
                                                <!-- Check all button -->
                                                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                                                </button>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i></button>
                                                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-reply"></i></button>
                                                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i></button>
                                                </div>
                                                <!-- /.btn-group -->
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i></button>
                                                <div class="float-right">
                                                    1-50/200
                                                    <div class="btn-group">
                                                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                                                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
                                                    </div>
                                                    <!-- /.btn-group -->
                                                </div>
                                                <!-- /.float-right -->
                                                </div>
                                                <div class="table-responsive mailbox-messages">
                                                <table class="table table-hover table-striped">
                                                    <tbody>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check1">
                                                        <label for="check1"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"></td>
                                                    <td class="mailbox-date">hace 5 min</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check2">
                                                        <label for="check2"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"></td>
                                                    <td class="mailbox-date">hace 28 min</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check3">
                                                        <label for="check3"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                                                    <td class="mailbox-date">hace 11 horas</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check4">
                                                        <label for="check4"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"></td>
                                                    <td class="mailbox-date">hace 15 horas</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check5">
                                                        <label for="check5"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                                                    <td class="mailbox-date">ayer</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check6">
                                                        <label for="check6"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                                                    <td class="mailbox-date">hace 2 días</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check7">
                                                        <label for="check7"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                                                    <td class="mailbox-date">hace 2 días</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check8">
                                                        <label for="check8"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"></td>
                                                    <td class="mailbox-date">hace 2 días</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check9">
                                                        <label for="check9"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"></td>
                                                    <td class="mailbox-date">hace 2 días</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check10">
                                                        <label for="check10"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"></td>
                                                    <td class="mailbox-date">hace 2 días</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check11">
                                                        <label for="check11"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                                                    <td class="mailbox-date">hace 4 días</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check12">
                                                        <label for="check12"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"></td>
                                                    <td class="mailbox-date">hace 12 días</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check13">
                                                        <label for="check13"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star-o text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                                                    <td class="mailbox-date">hace 12 días</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check14">
                                                        <label for="check14"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                                                    <td class="mailbox-date">hace 14 días</td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        <div class="icheck-primary">
                                                        <input type="checkbox" value="" id="check15">
                                                        <label for="check15"></label>
                                                        </div>
                                                    </td>
                                                    <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
                                                    <td class="mailbox-name"><a href="read-mail.html">Alexander Pierce</a></td>
                                                    <td class="mailbox-subject"><b>EduAPP</b> - Este es un mensaje de prueba...
                                                    </td>
                                                    <td class="mailbox-attachment"><i class="fas fa-paperclip"></i></td>
                                                    <td class="mailbox-date">hace 15 días</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!-- /.table -->
                                                </div>
                                                <!-- /.mail-box-messages -->
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-footer p-0">
                                                <div class="mailbox-controls">
                                                <!-- Check all button -->
                                                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                                                </button>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i></button>
                                                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-reply"></i></button>
                                                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i></button>
                                                </div>
                                                <!-- /.btn-group -->
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i></button>
                                                <div class="float-right">
                                                    1-50/200
                                                    <div class="btn-group">
                                                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                                                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
                                                    </div>
                                                    <!-- /.btn-group -->
                                                </div>
                                                <!-- /.float-right -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-enviar" role="tabpanel" aria-labelledby="nav-profile-tab">
                                        <div class="card card-primary card-outline ">
                                            <div class="card-header d-flex justify-content-between align-items-center">
                                                <h3 class="card-title">Listado de Participantes</h3>
                                                <div class="card-tools">
                                                    <div class="input-group input-group-sm">
                                                        <input type="text" class="form-control" placeholder="Buscar participante">
                                                        <div class="input-group-append">
                                                            <div class="btn btn-primary">
                                                                <i class="fas fa-search"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer p-0">
                                                <div class="mailbox-controls">
                                                <!-- Check all button -->
                                                    <div class="float-right">
                                                    1-50/200
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                                                        <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
                                                    </div>
                                                    <!-- /.btn-group -->
                                                    </div>
                                                    <center>Enviar aviso a todos los seleccionados:  <i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></center>
                                                    <!-- /.float-right -->
                                                </div>
                                            </div>

                                            <div class="card-body p-0">
                                                <div class="table-responsive ">

                                                    <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                                                        <thead>
                                                        <tr>
                                                            <th>N°</th>
                                                            <th align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></th>
                                                            <th>Apellidos</th>
                                                            <th>Nombre</th>
                                                            <th>Apoderado</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                        </thead>

                                                        <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Aguayo González</td>
                                                            <td>Jaime</td>
                                                            <td>Maria González</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>


                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Chávez Heredia</td>
                                                            <td>Andrea</td>
                                                            <td>Maria Heredia</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Cortés Lagunes</td>
                                                            <td>Ruth</td>
                                                            <td>Amanda Lagunes</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>de jesús Ramos</td>
                                                            <td>Ariana</td>
                                                            <td>Isabel Ramos</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>5</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Delgado Barrón</td>
                                                            <td>Luis</td>
                                                            <td>Carmen Barrón</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>6</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Espejo Ramos</td>
                                                            <td>Hansel</td>
                                                            <td>Francisca Ramos</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>7</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Flores Aguilar</td>
                                                            <td>Aniyensy</td>
                                                            <td>Ana Aguilar</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>8</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Flores Silva</td>
                                                            <td>Karla</td>
                                                            <td>Pilar Silva</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>García Arreguín</td>
                                                            <td>Montserrat</td>
                                                            <td>Laura Arreguín</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>García Orozco</td>
                                                            <td>Lisset</td>
                                                            <td>Juana Orozco</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Gómez Vargas</td>
                                                            <td>José</td>
                                                            <td>Rosario Vargas</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>12</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>González Díaz</td>
                                                            <td>Rocio</td>
                                                            <td>Elena Díaz</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>13</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>González Trejo</td>
                                                            <td>Cipriano</td>
                                                            <td>Marta Trejo</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>14</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Guerrero Padrés</td>
                                                            <td>Miguel</td>
                                                            <td>Rosa Padrés</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>15</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Guillen Marin</td>
                                                            <td>Karina</td>
                                                            <td>Raquel Marin</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>16</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Hernández González</td>
                                                            <td>Danna</td>
                                                            <td>Teresa González</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>17</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Hernández Palacios</td>
                                                            <td>Jaime</td>
                                                            <td>Angela Palacios</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>18</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Hernández Prado</td>
                                                            <td>Miguel</td>
                                                            <td>Julia Prado</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>19</td>
                                                            <td align="center"><input type="checkbox" id="cbox1" class="combo-box-ausencia" value="first_checkbox"></td>
                                                            <td>Herrera Arias</td>
                                                            <td>Luis</td>
                                                            <td>Alicia Arias</td>
                                                            <td align="center"><i class="ace-icon fa fa-envelope bigger-130 green" data-toggle="modal" data-target="#avisoModal" data-whatever="@getbootstrap"></i></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <!-- /.col -->
        </div>
    </div><!-- /.container-fluid -->
    <!--MensajesModal-->
    <div class="modal fade" id="avisoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Enviar aviso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Destinatarios: </label>
                            <input type="text" class="form-control" id="recipient-name" placeholder="Para: usuario1, usuario2">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Mensaje:</label>
                            <textarea class="form-control" id="message-text"></textarea>
                        </div>
                    </form>
                    <div class="form-group">
                        <div class="btn btn-default btn-file">
                            <i class="fas fa-paperclip"></i> Adjuntar
                            <input type="file" name="attachment">
                        </div>
                        <p class="help-block">Max. 50MB</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
                    <button type="button" class="btn btn-primary"><i class="far fa-envelope"></i> Enviar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

