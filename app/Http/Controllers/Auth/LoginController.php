<?php

namespace App\Http\Controllers\Auth;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;



class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

 
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

       // retorna a vista login
    public function showLoginForm(){

        return view('auth.login');
    }

    public function login(){
        
        $datos  = $this->validate(request(), [
            $this->username() => 'email|required|string',
            //auth()->user()->getAuthPassword() => 'required|string'
            $this->getAuthPassword() => 'required|string'
        ]);

      

        // valida si lo datos son correctos crea la sesion
        if (Auth::attempt(['email'=> $datos['email'] , 'password' => $datos['password'] ] )) {
            //
            return redirect('inicio');
        }

        // si son incorrectos devuelve un mensaje
        return back()
        ->withErrors([$this->username() =>  trans('auth.failed')])
        ->withInput(request([$this->username()]));
         //return $this->getAuthPassword();
    }

    // funcion para cerrar sesion
    public function logout(){
        // cierra sesion y devuelve al login 
        Auth::logout();

        return redirect('/');
    }

    
    public function getAuthPassword () {

        return "password";

    }
    
    

    public function username(){
        return "email";
    }
}
