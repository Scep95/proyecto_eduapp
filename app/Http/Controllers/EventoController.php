<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Evento;
//use Notify;

class EventoController extends Controller
{
    //
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function get_events(){
        $events = Evento::select("titulo as title", "fecha_inicio as start", "fecha_final as end","descripcion","color as backgroundColor","color as borderColor")->get()->toArray();
        return response()->json($events);
    }

    public function create_events(Request $request){
        $input = $request->all();
        //$request->currColor;

        $fechas = explode(" - ", $input["fechas"]);

        $input["fecha_inicio"] = $fechas[0];
        $input["fecha_final"] = $fechas[1];
        //$input["color"] = "#3c8dbc";


        Evento::create($input);
        //Notify::success("Se registro","felicidades");

        return redirect("/actividades");
    }

    public function index(){
        return view('actividades');
    }
}
